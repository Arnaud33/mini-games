var player1Points = 0;
var player2Points = 0;

var speed = 1.5;

document.addEventListener("DOMContentLoaded", () => {
  /* PLAYER 1 BAR MOVEMENTS */
  document.addEventListener("keydown", keyboard => {
    const $player1Bar = document.querySelector("#player1Bar");
    const $player2Bar = document.querySelector("#player2Bar");
    var $ball = document.querySelector(".ball");

    /* PLAYER 2 PRESS ARROWDOWN KEY */
    if (keyboard.key === "ArrowDown") {
      var sensY = 1;

      var currentPositionY = $player2Bar.offsetTop;
      var newPosition;
      if (currentPositionY > window.innerHeight - 100) {
        sensY = -1;
      } else if (currentPositionY < 0) {
        sensY = 1;
      }

      newPosition = currentPositionY + 30 * sensY;
      $player2Bar.style.top = newPosition + "px";

      /* PLAYER  2 PRESS ARROWUP KEY */
    } else if (keyboard.key === "ArrowUp") {
      var sensX = 1;

      var currentPositionY = $player2Bar.offsetTop;
      var newPosition;
      if (currentPositionY > 0) {
        sensY = 1;
      } else if (currentPositionY < 50) {
        sensY = -1;
      }

      newPosition = currentPositionY - 30 * sensY;
      $player2Bar.style.top = newPosition + "px";
    }

    /* PLAYER 1 BAR MOVEMENTS */

    /* PLAYER 1 PRESS S KEY */
    if (keyboard.key === "s" || keyboard.key === "S") {
      var sensY = 1;

      var currentPositionY = $player1Bar.offsetTop;
      var newPosition;
      if (currentPositionY > window.innerHeight - 100) {
        sensY = -1;
      } else if (currentPositionY < 0) {
        sensY = 1;
      }

      newPosition = currentPositionY + 30 * sensY;
      $player1Bar.style.top = newPosition + "px";


      /* PLAYER 1 PRESS Z KEY */
    } else if (keyboard.key === "z" || keyboard.key === "Z") {
      var sensX = 1;

      var currentPositionY = $player1Bar.offsetTop;
      var newPosition;
      if (currentPositionY > 0) {
        sensY = 1;
      } else if (currentPositionY < 50) {
        sensY = -1;
      }

      newPosition = currentPositionY - 30 * sensY;
      $player1Bar.style.top = newPosition + "px";
    }

    /* BALL MOVEMENTS */
    if (keyboard.key === " ") {
      var $ball = document.querySelector(".ball");

      /* BALL X AXE */
      var sensX = 1;
      setInterval(() => {
        var newPosition;
        var ball = $ball.getBoundingClientRect();
        var player1bar = $player1Bar.getBoundingClientRect();
        var player2bar = $player2Bar.getBoundingClientRect();

        if (ball.left > window.innerWidth - 25) {
          player1Points++;
          $ball.remove();

          alert("Point to Player 1 !");
          var scorePlayer1 = document.querySelector('#scorePlayer1');
          scorePlayer1.textContent = player1Points;

          var $ballnew = document.createElement("div");
          var body = document.querySelector("body");
          $ballnew.style.position = "fixed";
          $ballnew.style.bottom = "80px";
          $ballnew.style.left = "calc(50% - 12.5px)";
          $ballnew.style.backgroundColor = "red";
          $ballnew.style.width = "25px";
          $ballnew.style.height = "25px";
          $ballnew.style.borderRadius = "50%";
          $ballnew.classList = "ball";
          body.appendChild($ballnew);
          speed = 1.5;
        } else if (ball.left < 0) {
          player2Points++;
          $ball.remove();

          alert("Point to Player 2 !");
          var scorePlayer2 = document.querySelector('#scorePlayer2');
          scorePlayer2.textContent = player2Points;

          var $ballnew = document.createElement("div");
          var body = document.querySelector("body");
          $ballnew.style.position = "fixed";
          $ballnew.style.bottom = "80px";
          $ballnew.style.left = "50%";
          $ballnew.style.backgroundColor = "red";
          $ballnew.style.width = "25px";
          $ballnew.style.height = "25px";
          $ballnew.style.borderRadius = "50%";
          $ballnew.classList = "ball";
          body.appendChild($ballnew);
          speed = 1.5;

        } else if (
          ball.left < player1bar.left + player1bar.width &&
          ball.left + ball.width > player1bar.left &&
          ball.top < player1bar.top + player1bar.height &&
          ball.top + ball.height > player1bar.top
        ) {
          sensX = 1;
          speed += 0.1;
        } else if (
          ball.left < player2bar.left + player2bar.width &&
          ball.left + ball.width > player2bar.left &&
          ball.top < player2bar.top + player2bar.height &&
          ball.top + ball.height > player2bar.top
        ) {
          sensX = -1;
          speed += 0.1;
        }
        newPosition = ball.left + 1 * sensX * speed;
        $ball.style.left = newPosition + "px";
      }, 16);

      /* BALL Y AXE */
      var sensY = 1;
      setInterval(() => {

        var newPosition;
        var ball = $ball.getBoundingClientRect();

        if (ball.top > window.innerHeight - 20) {
          sensY = 1;
        } else if (ball.top < 0) {
          sensY = -1;
        }

        newPosition = ball.top - 1 * sensY * speed;
        $ball.style.top = newPosition + "px";
      }, 16);
    }
  });
});