console.log("Une balle rebondissante en Javascript");

var life = 5;

/* var spaceHasBeenPressed = false; */

document.addEventListener("DOMContentLoaded", () => {
  /* PLAYER'S BAR MOVEMENTS */

  /* BALL MOVEMENTS */
  document.addEventListener("keydown", keyboard => {
    /*  console.log(keyboard.key); */
    /*  document.addEventListener("keydown", keyboard => { */
    const $playerBar = document.querySelector("#playerBar");
    var $ball = document.querySelector(".ball");

    /* console.log(keyboard.key);
      console.log($playerBar.getBoundingClientRect()); */
    /* PLAYER PRESS ARROWRIGHT KEY */
    if (keyboard.key === "ArrowRight") {
      var sensX = 1;

      var currentPositionX = $playerBar.offsetLeft;
      var newPosition;
      if (currentPositionX > window.innerWidth - 150) {
        sensX = -1;
      } else if (currentPositionX < 0) {
        sensX = 1;
      }

      newPosition = currentPositionX + 30 * sensX;
      $playerBar.style.left = newPosition + "px";

      /* PLAYER PRESS ARROWLEFT KEY */
    } else if (keyboard.key === "ArrowLeft") {
      var sensX = 1;

      var currentPositionX = $playerBar.offsetLeft;
      var newPosition;
      if (currentPositionX > window.innerWidth - 150) {
        sensX = 1;
      } else if (currentPositionX < 0) {
        sensX = -1;
      }

      newPosition = currentPositionX - 30 * sensX;
      $playerBar.style.left = newPosition + "px";
    }
    /*  }); */

    if (keyboard.key === " ") {
      if (life > 0) {
        var $ball = document.querySelector(".ball");
        var $bricks = document.querySelectorAll("div div div");

        /* BALL X AXE */
        var sensX = 1;
        setInterval(() => {
          var currentPositionX = $ball.offsetLeft;
          var newPosition;

          if (currentPositionX > window.innerWidth - 60) {
            sensX = -1;
          } else if (currentPositionX < 0) {
            sensX = 1;
          }
          newPosition = currentPositionX + 1 * sensX;
          $ball.style.left = newPosition + "px";
        }, 1);

        /* BALL Y AXE */
        var sensY = 1;
        setInterval(() => {
          var currentPositionY = $ball.offsetTop;
          var newPosition;

          var ball = $ball.getBoundingClientRect();
          var playerbar = $playerBar.getBoundingClientRect();

          if (
            ball.left < playerbar.left + playerbar.width &&
            ball.left + ball.width > playerbar.left &&
            ball.top < playerbar.top + playerbar.height &&
            ball.top + ball.height > playerbar.top
          ) {
            sensY = 1;
          } else if (currentPositionY > window.innerHeight + 50) {
            life--;
            $ball.remove();
            if (life > 0) {
              alert(
                "you lost a life, you still have " + life + "lifes remaining."
              );
            } else {
              alert("Game Over ! ténul lol, va dormir");
            }
            var $ballnew = document.createElement("div");
            var body = document.querySelector("body");
            $ballnew.style.position = "fixed";
            $ballnew.style.bottom = "80px";
            $ballnew.style.left = "calc(50% - 12.5px)";
            $ballnew.style.backgroundColor = "red";
            $ballnew.style.width = "25px";
            $ballnew.style.height = "25px";
            $ballnew.style.borderRadius = "50%";
            $ballnew.classList = "ball";
            body.appendChild($ballnew);

            /* sensY = -1; */
          } else if (currentPositionY < 0) {
            sensY = -1;
          }

          newPosition = currentPositionY - 1 * sensY;
          $ball.style.top = newPosition + "px";
        }, 1);

        /* BRICKS BOUNCING ? */
        setInterval(() => {
          var currentPositionX = $ball.offsetLeft;
          var currentPositionY = $ball.offsetTop;
          var elementX;
          var elementY;
          $bricks.forEach(element => {
            var ball = $ball.getBoundingClientRect();
            var elementpos = element.getBoundingClientRect();
            /* elementX = element.getBoundingClientRect().left;
            elementY = element.getBoundingClientRect().top;
            elementWidth = element.getBoundingClientRect().width; */

            // console.log(
            //   "Element : " +
            //    element.getAttribute("id") +
            //     ": X = " +
            //     elementX +
            //     "| Y = " +
            //     elementY
            //  );
            /* if (currentPositionX > elementX) {
              sensX = -1;
            } else if (currentPositionX < 0) {
              sensX = 1;
            } */



            if (
              ball.left < elementpos.left + elementpos.width &&
              ball.left + ball.width > elementpos.left &&
              ball.top < elementpos.top + elementpos.height &&
              ball.top + ball.height > elementpos.top
            ) {
              sensY = -1;
              /* console.log(element); */
              element.style.display = "none";
            } /* else if (currentPositionY < 0) {
              sensY = -1;
            } */
          });
        }, 16);
      } else {
        alert("Game Over ! je t'ai dit quoi ? va dormir...");
      }
    }
  });
});
